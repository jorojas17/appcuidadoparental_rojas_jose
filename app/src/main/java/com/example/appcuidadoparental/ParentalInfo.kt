package com.example.appcuidadoparental

import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_parental_info.*

class ParentalInfo : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parental_info)

        val bundle:Bundle = intent.extras!!

        val area = bundle.getString("area")
        val titulo = bundle.getString("titulo")
        val exp = bundle.getString("exp")
        val ref = bundle.getString("ref")
        val imagen = bundle.getInt("imagen")


        TV_Area.text = area
        TV_Titulo.text = titulo
        txtExp.text = exp
        txtRef.text = ref
        imgInfo.setImageResource(imagen)




    }

}
