package com.example.appcuidadoparental

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat.*
import androidx.core.content.ContextCompat.startActivity as startActivity1


class AlarmReceiver: BroadcastReceiver()
{

    private lateinit var notificationManager: NotificationManager
    private  lateinit var notificationChannel: NotificationChannel
    val alarm : Boolean = false
    lateinit var builder : Notification.Builder
    val channelId = "notifications"
    val description = "My notification"

    override fun onReceive(context: Context, intent: Intent?) {

        val intent2 = Intent(context,MainActivity::class.java)
        val pendingIntent= PendingIntent.getActivity(context,0,intent2,PendingIntent.FLAG_UPDATE_CURRENT)

        notificationManager = context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = NotificationChannel(channelId,description,NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.GREEN
            notificationChannel.enableVibration(true)
            notificationManager.createNotificationChannel(notificationChannel)

            builder = Notification.Builder(context,channelId)
                .setContentTitle("Cuidado Parental")
                .setContentText("¿Preocupado por tus hijos? Consulta alguno de nuestro consejos para el cuidado parental!")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)

        }
        else
        {
            builder = Notification.Builder(context)
                .setContentTitle("Cuidado Parental")
                .setContentText("¿Preocupado por tus hijos? Consulta alguno de nuestro consejos para el cuidado parental!")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)

        }


        notificationManager.notify(0,builder.build())


    }


}