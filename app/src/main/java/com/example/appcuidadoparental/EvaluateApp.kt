package com.example.appcuidadoparental

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.RatingBar
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class EvaluateApp : AppCompatActivity() {


    private lateinit var txtEvaluate: EditText
    private lateinit var ratingB:RatingBar
    private lateinit var databaseReference: DatabaseReference
    private lateinit var database: FirebaseDatabase
    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_evaluate_app)

        txtEvaluate=findViewById(R.id.txtEvaluate)
        ratingB=findViewById(R.id.ratingBar)

        database = FirebaseDatabase.getInstance()
        auth = FirebaseAuth.getInstance()

        databaseReference = database.reference.child("User")




    }

    fun sendEvaluation(view:View)
    {
        newEvaluation()
    }

    fun newEvaluation()
    {
        val user:String=Preferencias.uid
        val rating:String?= ratingB.rating.toString()
        val evaluate:String?=txtEvaluate.text.toString()


        if(!TextUtils.isEmpty(evaluate) && !rating.isNullOrEmpty())
        {
                 val userDatabase = databaseReference.child(auth.currentUser?.uid!!)
                 userDatabase.child("Rating").setValue(rating)
                 userDatabase.child("Comentarios y/o Sugerencias").setValue(evaluate)

                     Toast.makeText(this, "Evaluacion enviada con exito! Muchas gracias!", Toast.LENGTH_SHORT).show()


                 GoToMain()

        }

    }

    fun GoToMain()
    {
        val intent = Intent(this,MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //Toast.makeText(this, "UID es: " + uid, Toast.LENGTH_SHORT).show()
        startActivity(intent)
    }
}
