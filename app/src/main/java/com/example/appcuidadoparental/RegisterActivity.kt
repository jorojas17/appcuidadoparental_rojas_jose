package com.example.appcuidadoparental

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class RegisterActivity : AppCompatActivity() {

    private lateinit var txtName:EditText
    private lateinit var txtLastname:EditText
    private lateinit var txtAge:EditText
    private lateinit var txtCity:EditText
    private lateinit var txtEmail:EditText
    private lateinit var txtPass:EditText
    private lateinit var progressBar: ProgressBar
    private lateinit var databaseReference: DatabaseReference
    private lateinit var database: FirebaseDatabase
    private lateinit var auth: FirebaseAuth

    private lateinit var preferencias : SharedPreferences




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        txtName=findViewById(R.id.txtName)
        txtLastname=findViewById(R.id.txtLastName)
        txtAge=findViewById(R.id.txtAge)
        txtCity=findViewById(R.id.txtCity)
        txtEmail=findViewById(R.id.txtUser)
        txtPass=findViewById(R.id.txtPassword)


        progressBar = findViewById(R.id.progressBar)

        database = FirebaseDatabase.getInstance()
        auth = FirebaseAuth.getInstance()

        databaseReference = database.reference.child("User")


    }

    fun register(view:View)
    {
        createNewUser()
    }

    fun createNewUser()
    {
        val name:String = txtName.text.toString()
        val lastname:String = txtLastname.text.toString()
        val age:String = txtAge.text.toString()
        val city:String = txtCity.text.toString()
        val email:String = txtEmail.text.toString()
        val pass:String = txtPass.text.toString()

        if(!TextUtils.isEmpty(name) && !TextUtils.isEmpty(lastname) && !TextUtils.isEmpty(age) && !TextUtils.isEmpty(city) && !TextUtils.isEmpty(email)
            && !TextUtils.isEmpty(pass))
        {
            progressBar.visibility= View.VISIBLE

            auth.createUserWithEmailAndPassword(email,pass).addOnCompleteListener(this)
            {
                task ->

                if(task.isComplete)
                {
                    val user:FirebaseUser?=auth.currentUser

                    user?.sendEmailVerification()?.addOnCompleteListener(this)
                    {
                            task ->
                        if(task.isComplete)
                        {
                            Toast.makeText(this,"Email enviado",Toast.LENGTH_LONG).show()
                            val userDatabase = databaseReference.child(user.uid)

                            userDatabase.child("Name").setValue(name)
                            userDatabase.child("LastName").setValue(lastname)
                            userDatabase.child("Age").setValue(age)
                            userDatabase.child("City").setValue(city)
                            userDatabase.child("Email").setValue(email)
                            userDatabase.child("Pass").setValue(pass)

                            Toast.makeText(this,"Usuario creado con exito!",Toast.LENGTH_LONG).show()

                            val preferencias = getSharedPreferences(Preferencias.DATOS, Context.MODE_PRIVATE)

                            val editor = preferencias.edit()
                            editor.putString(Preferencias.uid,user.uid)
                            editor.apply()



                            GoToMain(user.uid)
                        }
                        else
                        {
                            Toast.makeText(this,"Error al enviar el email",Toast.LENGTH_LONG).show()
                            progressBar.visibility= View.GONE
                        }
                    }



                }
            }
        }
    }



    private fun GoToMain(uid: String)
    {
        val intent = Intent(this,MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent)
    }





}
