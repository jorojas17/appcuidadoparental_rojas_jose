package com.example.appcuidadoparental



class ParentalCard
{
    var imagenInfo:Int?=null
    var areaInfo:String?=null
    var tituloInfo:String?=null
    var explicacionInfo:String?=null
    var referenciaInfo: String?=null



    constructor(img:Int,area:String,titulo:String,exp:String,ref:String)
    {

        this.imagenInfo = img
        this.areaInfo = area
        this.tituloInfo = titulo
        this.explicacionInfo = exp
        this.referenciaInfo = ref

    }
}
