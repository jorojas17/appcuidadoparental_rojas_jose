package com.example.appcuidadoparental

import android.app.*
import android.app.NotificationManager.IMPORTANCE_HIGH
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.icu.util.Calendar
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.parentalinfolist.view.*

import android.net.Uri
import android.os.Build
import android.view.*
import android.widget.*
import androidx.core.app.NotificationBuilderWithBuilderAccessor
import androidx.core.app.NotificationCompat
import java.nio.channels.Channel

import kotlin.collections.ArrayList




class MainActivity : AppCompatActivity() {


    private lateinit var preferencias: SharedPreferences
    private lateinit var calendar : Calendar
    private lateinit var notificationManager: NotificationManager
    private  lateinit var notificationChannel: NotificationChannel
    lateinit var builder : Notification.Builder
    val channelId = "notifications"
    val description = "My notification"
    lateinit var toolbar : androidx.appcompat.widget.Toolbar
    lateinit var alarm1: AlarmManager
    lateinit var con: Context
    lateinit var pi: PendingIntent



    //region Definicion de Lista de datos parentales y adaptador
    var listaDeDatosParentales = ArrayList<ParentalCard>()
    var myadapter:ParentalInfoAdapter?=null
    //endregion


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //region Preferences
        preferencias = getSharedPreferences(Preferencias.DATOS, Context.MODE_PRIVATE)

        if(preferencias.getString(Preferencias.uid,"").equals(""))
        {
            val intent = Intent(this,RegisterActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent)
        }
        //endregion

        //region Toolbar
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        //endregion

        //region Notificacion por dia
        this.con = this
        alarm1 = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val alarmIntent: Intent = Intent(this,AlarmReceiver::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val calendar: Calendar = Calendar.getInstance()
            calendar.set(Calendar.HOUR_OF_DAY, 18)
            calendar.set(Calendar.MINUTE, 20)
            calendar.set(Calendar.SECOND, 0)
            calendar.set(Calendar.MILLISECOND, 0)
            if (Calendar.getInstance().after(calendar)) {
                calendar.add(Calendar.DAY_OF_MONTH, 1)
            }
            pi = PendingIntent.getBroadcast(this, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            alarm1.setRepeating(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                AlarmManager.INTERVAL_DAY,
                pi
            )

        }




        //endregion

        //region Informacion Lista de Datos Parentales


        listaDeDatosParentales.add(ParentalCard(R.drawable.image5,"Crianza Efectiva","Autocuidado de padres para una crianza efectiva de los hijos e hijas","Ser padres y madres es una de las tareas más difíciles y desafiantes que existe; poder manejar la sobrecarga y el estrés en límites “normales” es esencial para poder desempeñar esta maravillosa labor. El Programa Aprender en Familia de Fundación CAP entrega algunas ideas para favorecer su autocuidado.El mensaje “cuidarnos para cuidar” tiene gran relevancia en todo ámbito y en particular en el rol de padres, uno de los desafíos más importantes y gratificantes que existen, pero al mismo tiempo, una de las tareas más complejas y exigentes.\n" +
                "\n" +
                "Ser padres y madres demanda tiempo completo de dedicación, cuidado, acompañamiento y atención. “A veces resulta bastante agotador, pues son muchos los temas, actividades y necesidades diferentes dependiendo de las edades de los hijos e hijas y en algunos momentos es normal que nos podamos sentir sobrepasados”, explica la psicóloga del Programa Aprender en Familia de Fundación CAP, Claudia Soto. “Lo importante es que esta sensación sea transitoria y no nos quedemos estancados en este agobio ya que si ocurre se pueden generar alteraciones en nuestro bienestar y salud tanto física como psicológica y también traer consecuencias importantes para toda la familia”, agrega.\n" +
                "\n" +
                "La experta comenta que este autocuidado podemos entenderlo como una capacidad parental de desarrollar conductas que promuevan una salud física y mental que se traduzca en mayor energía y recursos para ejercer las tareas de la crianza. Este autocuidado debe ser integral y tomarlo como una prioridad, ya que es fundamental para poder ejercer una parentalidad positiva. Por una parte nos ayuda a sentirnos y estar mejor lo que se traduce en más energía, disposición y paciencia para cumplir este rol y también nos permite transmitirle a nuestros hijos e hijas la importancia de cuidarse para vivir mejor, mostrando que ellos también deben protegerse, cuidarse y autoregularse en todas las etapas de sus vidas,  aclara: “Es importante que los padres y madres estemos atentos y cuidemos que el cansancio que experimentamos sea transitorio y podamos manejarlo, sabiendo que es normal sentirlo en algunas ocasiones”. Asegura que la actividad física, la dieta balanceada, hacer alguna actividad que nos guste y que no tenga relación con las tareas de crianza, el compartir tiempo con los amigos, son elementos fundamentales y plantea algunas ideas pare tener en consideración:\n" +
                "\n" +
                " ->Busca un espacio para realizar alguna actividad concreta y regular que te distraiga y te contacte contigo mismo, como juntarse con los amigos y amigas, ir al cine, caminar, leer, hacer manualidades, etc. \n" +
                " ->Gestiona bien tu tiempo y organiza tus tareas, priorizando algunas actividades y postergando otras.\n" +
                " ->Aprende a decir que no y respeta tus límites para que los demás los respeten.\n" +
                " ->Incorpora en tu rutina diaria alguna actividad o pausa que te relaje y te guste. Recuerda, si no nos cuidamos, ponemos en riesgo nuestro propio bienestar y también el de nuestra familia.\n" +
                " ->Pide ayuda. Cuando lo necesites y puedas, pide a familiares y amigos que te apoyen en funciones relacionadas con el cuidado de tus hijos.\n" +
                " ->Genera acciones para el cuidado de tu cuerpo, como hacer ejercicio, mantener una alimentación sana y dormir adecuadamente.\n" +
                " ->Aprende a escuchar tu cuerpo. Pon atención a dolores y malestares; si estos son recurrentes, consulta a un médico.","Fundación Cap 2020"))


        //Aca
        listaDeDatosParentales.add(ParentalCard(R.drawable.image6,"Crianza Efectiva","Estimule la autoestima de su hijo","Los niños comienzan a desarrollar su sentido del yo desde que son bebés, cuando se ven a sí mismos a través de los ojos de sus padres. Sus hijos asimilan su tono de voz, su lenguaje corporal y todas sus expresiones. Sus palabras y acciones como padre tienen un impacto en el desarrollo de su autoestima más que ninguna otra cosa. El elogio de los logros, aunque sean pequeños, hará que los niños estén orgullosos; permitirles que hagan cosas por sí solos los hará sentir que son capaces y fuertes. Por el contrario, los comentarios denigrantes o las comparaciones negativas con otros niños los hará sentir inútiles.\n" +
                "\n" +
                "Evite las afirmaciones tendenciosas o usar palabras hirientes. Los comentarios tales como \"¡Qué estupidez!\" o \"¡Te comportas más como si fueras un bebé que tu hermano pequeño!\" pueden causar el mismo daño que los golpes físicos. Elija las palabras con cuidado y sea compasivo. Dígales a sus hijos que todas las personas cometen errores y que usted aún los ama, incluso cuando no apruebe su comportamiento.","Revisado por: Steven Dowshen, MD\n" +
                "Fecha de revisión: enero de 2015"))

        //Aca
        listaDeDatosParentales.add(ParentalCard(R.drawable.image7,"Crianza Efectiva","Reconozca las buenas acciones","¿Se detuvo a pensar alguna vez cuántas veces al día tiene reacciones negativas para con sus hijos? Es posible que se dé cuenta de que los critica muchas más veces de las que los felicita. ¿Cómo se sentiría si un jefe lo tratara de un modo tan negativo, incluso si fuese con buenas intenciones?\n" +
                "\n" +
                "El enfoque más positivo es reconocer las buenas acciones de los niños: \"Hiciste la cama sin que te lo pidiera, ¡eso es genial!\" o \"Te estaba mirando mientras jugabas con tu hermana y fuiste muy paciente\". Estos comentarios serán mucho más eficaces para alentar la buena conducta a largo plazo que las reprimendas continuas.\n" +
                "\n" +
                "Propóngase encontrar algo para elogiar todos los días. Sea generoso con las recompensas: su amor, sus abrazos y elogios pueden hacer maravillas y suelen ser suficiente gratificación. Pronto descubrirá que está \"cultivando\" en mayor medida el comportamiento que desearía ver.","Revisado por: Steven Dowshen, MD\n" +
                "Fecha de revisión: enero de 2015"))

        //Aca
        listaDeDatosParentales.add(ParentalCard(R.drawable.image8,"Crianza Efectiva","Establezca límites y sea coherente con la disciplina","En todas las casas es necesaria la disciplina. El objetivo de la disciplina es ayudar a que los niños elijan los comportamientos aceptables y aprendan a autocontrolarse. Es posible que pongan a prueba los límites que usted establece, pero son imprescindibles para que ellos se conviertan en adultos responsables.\n" +
                "\n" +
                "Poner reglas en la casa ayuda a que los niños entiendan sus expectativas y desarrollen el autocontrol. Algunas reglas pueden incluir, por ejemplo, no mirar televisión hasta que estén hechas las tareas y no permitir los golpes, los insultos ni las burlas hirientes.\n" +
                "\n" +
                "Es recomendable que implemente un sistema: una advertencia seguida de consecuencias, que pueden ser una penitencia o la pérdida de privilegios. Un error frecuente que cometen los padres es no seguir adelante con las consecuencias. No puede disciplinar a los niños por una mala contestación un día e ignorar el hecho al día siguiente. Ser consistente les enseña qué es lo que usted espera.","Revisado por: Steven Dowshen, MD\n" +
                "Fecha de revisión: enero de 2015"))

        listaDeDatosParentales.add(ParentalCard(R.drawable.image9,"Crianza Efectiva","Hágase un tiempo para sus hijos","A menudo es difícil que los padres y los niños se reúnan para una comida en familia, ni pensar en que pasen juntos tiempo de calidad. Sin embargo, es probable que no haya nada que a los niños les gustaría más que eso. Levántese 10 minutos antes a la mañana para poder desayunar junto a sus hijos o deje los platos en el fregadero y salga a caminar después de cenar. Los niños que no reciben la atención que desean de sus padres a menudo sobreactúan o se comportan mal porque, de ese modo, están seguros de que recibirán su atención.\n" +
                "\n" +
                "Muchos padres descubren que es gratificante programar tiempo para pasar con sus hijos. Programe una \"noche especial\" cada semana para estar juntos y deje que sus hijos ayuden a decidir cómo pasar el tiempo. Busque otras formas de relacionarse, por ejemplo, ponga una nota o algo especial en las loncheras de los niños.\n" +
                "\n" +
                "Los adolescentes parecen necesitar menos atención individual de sus padres en comparación con los niños más pequeños. Puesto que hay menos oportunidades de que padres y adolescentes pasen tiempo juntos, los padres deben hacer su mayor esfuerzo para estar disponibles cuando sus hijos expresan el deseo de hablar o participar en actividades familiares. Asistir a conciertos, juegos y otros eventos con el adolescente es una forma de transmitir afecto, y le permite a usted conocer otros aspectos sobre su hijo y sus amigos que son importantes.\n" +
                "\n" +
                "No se sienta culpable si es un padre que trabaja. Los niños recordarán las pequeñas cosas que usted hace, por ejemplo, preparar palomitas de maíz, jugar a los naipes, mirar vidrieras.","Revisado por: Steven Dowshen, MD\n" +
                "Fecha de revisión: enero de 2015"))

        listaDeDatosParentales.add(ParentalCard(R.drawable.image10,"Crianza Efectiva","Sea un buen modelo a seguir","Los niños pequeños aprenden mucho sobre cómo actuar al observar a sus padres. Cuanto más pequeños, más lo imitan. Antes de reaccionar agresivamente o enfurecerse frente a su hijo, piense en lo siguiente: ¿es así como desea que el niño se comporte al enfadarse? Esté siempre consciente de que sus hijos lo están observando. Los estudios han demostrado que, por lo general, los niños que dan golpes imitan el modelo de agresión de sus casas.\n" +
                "\n" +
                "Sirva de ejemplo de las cualidades que desea cultivar en sus hijos: respeto, cordialidad, honestidad, amabilidad, tolerancia. Sea generoso. Haga cosas por los demás sin esperar una retribución. Exprese su agradecimiento y haga elogios. Por sobre todo, trate a sus hijos del mismo modo que espera que otras personas lo traten a usted.","Revisado por: Steven Dowshen, MD\n" +
                "Fecha de revisión: enero de 2015"))

        //Aca
        listaDeDatosParentales.add(ParentalCard(R.drawable.image11,"Crianza Efectiva","Haga de la comunicación una prioridad","No puede esperar que los niños hagan todo sólo porque usted como padre \"así lo dice\". Ellos desean y merecen explicaciones al igual que los adultos. Si no dedicamos tiempo a dar explicaciones, los niños comenzarán a cuestionarse nuestros valores y motivaciones, y si estos tienen fundamentos. Los padres que razonan con sus hijos les permiten entender y aprender sin emitir juicios de valor.\n" +
                "\n" +
                "Deje en claro sus expectativas. Si hay un problema, descríbalo, exprese sus sentimientos e invite a su hijo a que busquen juntos una solución. No olvide mencionar las consecuencias. Haga sugerencias y ofrezca alternativas. Además, esté dispuesto a escuchar las sugerencias de su hijo. Negocie. Los niños que participan en la toma de decisiones están más motivados a llevarlas adelante.","Revisado por: Steven Dowshen, MD\n" +
                "Fecha de revisión: enero de 2015"))

        listaDeDatosParentales.add(ParentalCard(R.drawable.image12,"Crianza Efectiva","Sea flexible y esté dispuesto a adaptar su estilo de crianza\n","Si el comportamiento de su hijo lo decepciona con frecuencia, quizás se deba a que sus expectativas no son realistas. Para los padres que piensan en \"lo que se debe\" (por ejemplo, \"A esta altura, mi hijo debe usar el orinal\"), puede ser útil leer sobre el tema o hablar con otros padres o con especialistas de desarrollo infantil.\n" +
                "\n" +
                "El entorno que rodea a los niños tiene un impacto en su comportamiento; por lo tanto, puede cambiar ese comportamiento si modifica el entorno. Si continuamente tiene que decirle \"no\" a su hijo de 2 años, busque algún modo de reestructurar el entorno para que haya menos cosas prohibidas. Esto será menos frustrante para ambos.\n" +
                "\n" +
                "A medida que su hijo cambie, tendrá que modificar gradualmente su estilo de crianza. Lo más probable es que lo que hoy resulta eficaz con su hijo ya no lo sea tanto en uno o dos años.\n" +
                "\n" +
                "Los adolescentes suelen buscar más modelos a seguir en sus pares y menos en sus padres. Sin embargo, no deje de orientar y alentar a su hijo adolescente ni de impartir la disciplina adecuada mientras que, a la vez, le permite independizarse cada vez más. Y aproveche todos los momentos que tenga para entablar una relación.","Revisado por: Steven Dowshen, MD\n" +
                "Fecha de revisión: enero de 2015"))

        listaDeDatosParentales.add(ParentalCard(R.drawable.image13,"Crianza Efectiva","Demuestre que su amor es incondicional","Como padre, usted tiene la responsabilidad de corregir y guiar a sus hijos. Sin embargo, la forma en que expresa su orientación correctiva tiene una gran influencia en la forma en la que un niño la recibe. Cuando tenga que enfrentarse a su hijo, evite echar culpas, hacer críticas o buscar defectos; todo esto puede debilitar la autoestima y provocar resentimiento. En cambio, haga un esfuerzo por educar y alentar, incluso cuando discipline a sus hijos. Asegúrese de que ellos sepan que, aunque desea y espera algo mejor la próxima vez, su amor es incondicional.","Revisado por: Steven Dowshen, MD\n" +
                "Fecha de revisión: enero de 2015"))

        listaDeDatosParentales.add(ParentalCard(R.drawable.image14,"Crianza Efectiva","Esté consciente de sus propias necesidades y limitaciones como padre","Enfréntelo: usted no es un padre perfecto. Como jefe de familia, tiene fortalezas y debilidades. Reconozca sus habilidades: \"Soy cariñoso y dedicado\". Prometa trabajar en sus debilidades: \"Debo ser más coherente con la disciplina\". Intente tener expectativas realistas para usted, su cónyuge y sus hijos. No es necesario que sepa todas las respuestas: sea indulgente con usted mismo.\n" +
                "\n" +
                "E intente que la crianza de los hijos sea una labor que se pueda manejar. Concéntrese en las áreas que necesitan la mayor atención, en lugar de intentar abordar todo a la vez. Admita cuando se sienta agotado. Quítele tiempo a la crianza para hacer cosas que lo harán sentir feliz como persona (o como pareja).\n" +
                "\n" +
                "Centrarse en sus necesidades no lo convierte en una persona egoísta. Simplemente quiere decir que se preocupa por su propio bienestar, otro valor importante para que sus hijos tomen como ejemplo a seguir.","Revisado por: Steven Dowshen, MD\n" +
                "Fecha de revisión: enero de 2015"))

        listaDeDatosParentales.add(ParentalCard(R.drawable.image15,"Redes Sociales","¿Quién no tiene hoy en día al menos una red social abierta? Las maneras de comunicarnos en los últimos tiempos ha cambiado considerablemente, y el canal más habitual son las redes sociales. Exponernos en Internet es hacerlo al mundo entero, por eso es fundamental saber a qué edad deberían entrar en contacto con ellas nuestros hijos. Hay que tener especial cuidado con los menores y con el contenido que circula por la red.","Buen uso de redes sociales\n" +
                "Si bien la mayoría de las redes sociales impone como norma que no la utilicen menores de 13 años, lo cierto es que cada vez más niños en edad escolar se registran, comparten fotos y videos con sus amigos y participan activamente online. El mundo virtual es parte de su vida cotidiana –aún más que de la nuestra, sus padres– y es fundamental saber protegerlos de los riesgos que puedan correr.\n" +
                "\n" +
                "Siempre bajo tu supervisión\n" +
                "De nada sirve prohibir el ordenador. Es más, en la era en la que nos encontramos es necesario que lo sepan manejar, pero siempre con criterio. Es preferible que tu hijo sepa que puede usarlo, pero que tú estás al tanto de lo que él hace. Procura que los dispositivos estén en un lugar de uso común (por ejemplo, en el salón) para evitar que los niños pasen tiempo encerrados en su dormitorio sin que sepas qué están haciendo online.Y aprende a utilizar las diferentes herramientas de control parental.\n" +
                "\n" +
                "Mantén la cuenta privada\n" +
                "Cuando de redes sociales se trata, es importante que les enseñes a tus niños a configurar sus perfiles de modo que todo lo que publiquen sea privado, es decir, que únicamente sus contactos tengan acceso a ello.De cualquier manera hay que evitar proporcionar datos tales como direcciones, números telefónicos o localización en tiempo real. Un niño es muy fácil de persuadirl, por lo que conviene también controlar el tipo de conversaciones que se mantienen en los chats de las redes.\n" +
                "\n" +
                "Sólo aceptar amigos “reales”\n" +
                "El típico ejemplo de «no hables ni aceptes nada de desconocidos» hay que aplicarlos al mundo virtual. Explica a tus hijos con qué facilidad un adulto desconocido podría hacerse pasar por un niño de su edad. Por eso es fundamental que únicamente acepten como contactos a amigos y familiares que conozcan y con quienes tengan confianza fuera de la pantalla. Es más: no deberían aceptar a nadie antes de que la persona les confirme que efectivamente les haya mandado una solicitud de contacto.\n" +
                "\n" +
                "Enséñales a bloquear usuarios\n" +
                "Si un desconocido intenta abordarlos en una red social, si les deja mensajes (ofensivos o no) y no se da por vencido frente a la negativa de ellos a aceptarlo como contacto, los niños deberían notificártelo a ti antes que a nadie, y después, reportarlo o bloquearlo en la red social.\n" +
                "\n" +
                "Cuidar su “reputación virtual”\n" +
                "Tal vez el límite más difícil y el más importante a trabajar con los niños es que comprendan que todo lo que ellos publican (o que otros publican acerca de ellos) en Internet es factible de volverse viral, de ser copiado y reproducido –y, eventualmente, de ser utilizado en su contra. Por ello es crucial cuidarse muy bien de dejar comentarios ofensivos, de subir determinadas fotos que atenten contra su privacidad, o de involucrarse en peleas o discusiones virtuales.\n" +
                "\n" +
                "Asimismo, si ellos son víctima de un ataque (o de “bullying” virtual) es necesaria tu intervención. No debes dejarlo pasar como “cosas de niños” porque lo cierto es que puede escalar y llegar a perjudicarlos en grande.\n" +
                "\n" +
                "Las redes sociales pueden ser una excelente herramienta de comunicación, de socialización y de ocio, siempre y cuando las utilicemos con los debidos cuidados.","Revisado por: Steven Dowshen, MD\n" +
                "Fecha de revisión: enero de 2015"))

                listaDeDatosParentales.add(ParentalCard(R.drawable.image16,"Trauma Infantil","El rechazo. El miedo al rechazo surge tan pronto como el niño se da cuenta de que es una persona independiente de sus padres, aproximadamente a los dos años de edad.","En ese momento, el niño comienza a buscar activamente la aceptación de las figuras que son importantes para él. Si estas personas le rechazan, puede generarse un trauma infantil, una herida emocional difícil de cicatrizar ya que genera la creencia de que no es suficientemente bueno ni digno de ser amado. El rechazo en la infancia provoca la descalificación hacia uno mismo y genera una baja autoestima. Se trata de personas que tienen continuamente miedo a fracasar y que necesitan imperiosamente la aprobación de los demás. \n" +
                "¿Cómo sanar esta herida?\n" +
                "\n" +
                "Comienza a valorar tus habilidades positivas y logros. Poco a poco, atrévete a arriesgar y tomar decisiones por ti mismo. Te darás cuenta que a medida que ganas seguridad, la opinión de los demás deja de condicionarte. De esta forma, comenzarás a vivir más plenamente, haciendo lo que de verdad te gusta y apasiona.","Unidos Por la Vida (2016). Papel de la Familia. Disponible en\n" +
                        "http://www.unidosporlavida.com/"))

                listaDeDatosParentales.add(ParentalCard(R.drawable.image17,"Trauma Infantil","El abandono. Los niños necesitan a otras personas para crecer, solo a través de ese contacto se forma adecuadamente su personalidad.","Sin embargo, si sus padres siempre han estado ausentes, aunque sea desde el punto de vista emocional, ese niño se sentirá abandonado, no tendrá un apoyo a quien recurrir cuando lo necesite. Por eso, las personas que han vivido experiencias de abandono en su infancia, suelen ser inseguras y desarrollan una dependencia emocional, basada en un profundo miedo a que les vuelvan a abandonar. En el fondo, no han logrado deshacerse de ese trauma infantil, y lo siguen reviviendo en sus relaciones cotidianas.\n" +
                        "¿Cómo sanar esta herida?\n" +
                        "\n" +
                        "Ante todo, es importante que aprendas a estar a gusto contigo mismo. No es necesario que siempre tengas a personas a tu alrededor, a veces, la soledad es buena consejera. Recuerda que a lo largo de la vida, nos encontramos con muchas personas y es normal que en cierto punto nuestros caminos se separen. Aprende a abrazar los cambios y desarrolla una visión optimista de las relaciones interpersonales, es posible que al doblar de la esquina haya alguien fabuloso esperando conocerte.","Unidos Por la Vida (2016). Papel de la Familia. Disponible en\n" +
                        "http://www.unidosporlavida.com/"))

                listaDeDatosParentales.add(ParentalCard(R.drawable.image18,"Trauma Infantil","La humillación. Se ha demostrado que el rechazo y la humillación social, no solo provocan sufrimiento sino un dolor a nivel físico ya que esta sensación comparte los mismos circuitos cerebrales que el dolor.","La humillación ya resulta difícil de sobrellevar para un adulto, por lo que para un niño puede ser una herida atroz. De hecho, es probable que aún recuerdes un hecho de tu infancia en el que te sentiste humillado. Si esa situación se repite con frecuencia, es probable que la persona termine desarrollando un mecanismo de defensa que la convierta en un ser tiránico y egoísta, se trata de una coraza para defenderse de humillaciones futuras.\n" +
                        "¿Cómo sanar esta herida?\n" +
                        "\n" +
                        "En este caso, es importante aprender a perdonar. Solo cuando dejamos ir el rencor que hemos guardado durante años, podemos encontrar nuestro verdadero “yo”, que no es un niño asustado que necesita defenderse sino un adulto seguro de sí, que conoce sus capacidades y no duda en defender sus derechos de forma asertiva.","Unidos Por la Vida (2016). Papel de la Familia. Disponible en\n" +
                "http://www.unidosporlavida.com/"))

                 listaDeDatosParentales.add(ParentalCard(R.drawable.image19,"Trauma Infantil","La injusticia. Hace poco se descubrió que los niños muy pequeños, de apenas 15 meses, ya tienen un sentido de la justicia lo suficientemente desarrollado como para catalogar una situación como desigual o igualitaria. ","Por eso, recibir una educación en la que han sido víctimas de injusticias constantes, lacera profundamente su “yo”, transmitiéndoles la idea de que no son merecedores de la atención de los demás. Un adulto que sufrió injusticias de niño puede convertirse en una persona insegura o, al contrario, en alguien cínico que tiene una visión pesimista de la vida. Debido a ese trauma infantil, esta persona tendrá problemas para confiar en los demás y establecer relaciones porque, inconscientemente, piensa que todos le tratarán mal.\n" +
                         "¿Cómo sanar esta herida?\n" +
                         "\n" +
                         "Es importante aceptar que las injusticias que se hayan cometido en la infancia, no tienen por qué repetirse en la adultez. Comprende que ahora cuentas con otros recursos para hacer valer tus derechos y recibir un trato mucho más justo.","Unidos Por la Vida (2016). Papel de la Familia. Disponible en\n" +
                "http://www.unidosporlavida.com/"))

                listaDeDatosParentales.add(ParentalCard(R.drawable.image20,"Trauma Infantil","La traición. Una de las cosas que no perdonan los niños, es haber sido traicionados, sobre todo por sus padres. ","Sin embargo, se trata de una situación bastante común ya que muchos padres hacen promesas que luego no cumplen. De esta forma, generan en el niño la idea de que el mundo es un sitio poco fiable. Sin embargo, si no logramos confiar en las personas, nos convertimos en ermitaños, aislados del mundo, que nunca podrán lograr nada y que se sentirán profundamente solos. Estas personas normalmente se comportan de manera fría, intentan construir un muro en sus relaciones interpersonales y no dejan que los demás entren en su intimidad.\n" +
                        "¿Cómo sanar esta herida?\n" +
                        "\n" +
                        "El hecho de que las personas en las que debías confiar te hayan defraudado, no significa que todos lo harán. Para construir relaciones sólidas, es necesario dejar entrar a los demás en tu vida y confiar en ellos. Solo cuando eres capaz de entregarte, los demás se entregarán a ti.","Unidos Por la Vida (2016). Papel de la Familia. Disponible en\n" +
                "http://www.unidosporlavida.com/"))


        listaDeDatosParentales.add(ParentalCard(R.drawable.image1,"Crianza severa puede alterar el desarrollo cerebral","Una baja sensibilidad parental y una crianza severa y negativa pueden afectar el " +
                "desarrollo cerebral del niño en la forma en cómo organiza su conducta.","¿Qué significa que una crianza severa pueda alterar el desarrollo cerebral? \n" +"El " +
                "desarrollo de las habilidades cognitivas complejas (funciones ejecutivas cerebrales) comienza" +
                "dentro de los primero 5 años del niño. En paralelo, se desarrolla la corteza pre frontal del" +
                "cerebro, la cual orienta la forma de organizar la propia conducta en el niño ¿Suena importante" +
                "no?. Estudios han demostrado que la experiencia ambiental, como por ejemplo la parentalidad," +
                "influye en el desarrollo de esta corteza (Bumm!) y claro dentro de los primeros años del niño," +
                "éste depende única y exclusivamente de sus cuidadores. Siendo la sensibilidad del cuidador, uno" +
                "de los elementos centrales de la parentalidad, definida como la capacidad de interpretar de" +
                "manera adecuada las señales del niño y responder a ellas de manera atingente. \n" +
                " \n Entonces…¿Qué elementos afectan el funcionamiento cerebral del niño? Un estudio señala que" +
                "la baja sensibilidad parental y una crianza severa (actos coercitivos y expresiones emocionales" +
                "negativas dirigidas a los niños) están relacionadas con bajos puntajes obtenidos por el niño en" +
                "la habilidad de entender la manera en que piensa y aprende (meta cognición) y controlar las" +
                "respuestas impulsivas y generar respuestas mediadas por la atención y el razonamiento (auto" +
                "control inhibitorio)."," Lucassen, N., Kok, R., Bakermans‐Kranenburg, M. J., Van Ijzendoorn, M. H., Jaddoe," +
                "V. W., Hofman, A., ... & Tiemeier, H. (2015). Executive functions in early childhood: The role of" +
                "maternal and paternal parenting practices. British Journal of Developmental Psychology, 33(4)," +
                "489-505."))

        listaDeDatosParentales.add(ParentalCard(R.drawable.image2,"Competencia Emocional"," ¿Por qué es importante incorporar el lenguaje emocional? La actitud y respuesta que el " +
                "cuidador tiene de las emociones del niño juega un rol importante en la manera de enseñar sobre " +
                "las emociones y al mismo tiempo influencia la forma en cómo el niño expresa sus propias " +
                "emociones.", "Diversos estudios han demostrado que los vínculos y experiencias que tienen niños " +
                "y niñas con sus familias, cuidadores, profesores y referentes cercanos, tienen influencia directa " +
                "en su expresión y regulación emocional. Donde se ha revisado que aquellos niños y niñas que " +
                "reciben un modelado positivo sobre expresión y manejo de emociones de sus figuras parentales, " +
                "también adquieren mayor manejo y expresión apropiada de sus emociones. Además aquellas " +
                "figuras parentales que presentan dificultades para identificar sus emociones (ira, tristeza) " +
                "pueden tener mayores dificultades para detectar las emociones de sus hijos e hijas.", " Havighurst, S. S., Wilson, K. R., Harley, A. E., & Prior, M. R. (2009). Tuning in to kids: " +
                "an emotion‐focused parenting program—initial findings from a community trial. Journal of " +
                "Community Psychology, 37(8), 1008-1023."))

        listaDeDatosParentales.add(ParentalCard(R.drawable.image3,"Trauma infantil"," Estrés en los niños y funciones de la parentalidad"," Los cuidadores pueden modular las respuestas al estrés en los niños a través de la " +
                "seguridad del vínculo en la relación que posean. Lo que permite controlar la respuesta fisiológica " +
                "y comportamental del niño. Siendo este apoyo uno de los pilares del desarrollo de la regulación " +
                "emocional. ¿A través de qué? El vínculo de apego seguro es identificado como un mediador en " +
                "las respuestas al estrés. El poder de las relaciones de apego seguras sirve para amortiguar o " +
                "prevenir aumentos en el nivel de cortisol del niño. \n" + "\n Las dinámicas familiares también influencian el desarrollo de la reactividad del cortisol en niños. " +
                "En estudios de observación en ambientes naturales en niños de 2 meses a 17 años arrojan " +
                "evidencia de que los eventos familiares traumáticos (conflicto, castigo, vergüenza, peleas graves " +
                "y peleas) están fuertemente asociados con períodos de niveles elevados de cortisol, en " +
                "comparación a sus propios niveles en días menos caóticos en la familia (Flinn e England 1995). ","Gunnar, M., & Quevedo, K. (2007). The neurobiology of stress and development. Annu. Rev. " +
                "Psychol., 58, 145-173."))

        listaDeDatosParentales.add(ParentalCard(R.drawable.image4,"Trauma infantil"," Las respuestas al trauma infantil son mediadas por el nivel de desarrollo y apoyo parental","-> Después de experimentar estos hechos traumáticos muchos niños son resilientes y no " +
                "generan síntomas de trauma.\n" +
                "-> Niños menores dependen más de las reacciones de los padres al trauma que niños " +
                "mayores.\n" +
                "-> Si los padres reaccionan bien, los niños menores no desarrollan necesariamente " +
                "síntomas traumáticos a largo plazo.\n" +
                "-> Si el trauma se vivió muy tempranamente, puede alterar dramáticamente la trayectoria " +
                "del desarrollo, más que haber vivido trauma crónico en la adolescencia.","Schnyder, U., & Cloitre, M. (Eds.). (2015). Evidence based treatments for traumarelated psychological disorders: A practical guide for clinicians. Springer."))




        //endregion



        //region Adaptador de layout
        myadapter = ParentalInfoAdapter(this,listaDeDatosParentales)
        listaParentalLayout.adapter = myadapter
        //endregion

    }

    //region Menu Functions
    override fun onCreateOptionsMenu(menu: Menu?): Boolean
    {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.buttons,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var selectedOption = ""

        when(item?.itemId)
        {
            R.id.email -> selectedOption = "Mail"
            R.id.wsp -> selectedOption = "Wsp"
            R.id.evaluar -> selectedOption = "Evaluar"


        }

        if(selectedOption.equals("Mail"))
        {
            sendemail("Consulta de cuidado parental")
        }
        if(selectedOption.equals("Wsp"))
        {
            whatsapp()
        }
        if(selectedOption.equals("Evaluar"))
        {
            GoToEvaluate()
        }

        return super.onOptionsItemSelected(item)
    }
    //endregion

    //region Contact Functions
    fun whatsapp() {
        try {

            val text = "Hola! Me gustaria hacer una consulta sobre cuidado parental."// Replace with your message.

            val toNumber =
                "56965646521"

            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("http://api.whatsapp.com/send?phone=$toNumber&text=$text")
            startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    fun sendemail(subject: String) {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto: cuidadoparental2020@gmail.com") // only email apps should handle this

            putExtra(Intent.EXTRA_SUBJECT, subject)
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    //endregion

    //region GoToFunctions

    fun GoToEmail(view:View)
    {
        sendemail("Consulta sobre cuidado parental")
    }

    fun GoToWsp(view:View)
    {
        whatsapp()
    }

    fun GoToEvaluate()
    {
        val intent = Intent(this,EvaluateApp::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //Toast.makeText(this, "UID es: " + uid, Toast.LENGTH_SHORT).show()
        startActivity(intent)
    }

    //endregion

    //region Adaptador de Layout
    inner class ParentalInfoAdapter:BaseAdapter {

        var listaDeDatosParentales = ArrayList<ParentalCard>()
        var context: Context? = null

        constructor(context: Context, ldp: ArrayList<ParentalCard>) : super() {
            this.listaDeDatosParentales = ldp
            this.context = context

        }


        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

            var myView:View? = null
            val infoParental = this.listaDeDatosParentales[position]
            var inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

            myView = inflator.inflate(R.layout.parentalinfolist,null)

            myView.TV_Area.text = infoParental.areaInfo
            myView.TV_Titulo.text = infoParental.tituloInfo
            myView.imageView.setImageResource(infoParental.imagenInfo!!)


            listaParentalLayout.setOnItemClickListener { parent: AdapterView<*>?, view: View?, position, id ->

                val parentalDatacard = listaDeDatosParentales[position]
                val intent = Intent(context, ParentalInfo::class.java)

                intent.putExtra("area", parentalDatacard.areaInfo)
                intent.putExtra("titulo", parentalDatacard.tituloInfo)
                intent.putExtra("exp", parentalDatacard.explicacionInfo)
                intent.putExtra("ref", parentalDatacard.referenciaInfo)
                intent.putExtra("imagen", parentalDatacard.imagenInfo!!)

                context!!.startActivity(intent)
            }


            return myView
        }

        override fun getItem(position: Int): Any {
            return listaDeDatosParentales[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return listaDeDatosParentales.size
        }
    }
    //endregion



}
